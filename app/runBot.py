#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from discord import File
from discord.ext.commands import Bot

# Discord client
client = Bot(command_prefix="g!")

IMG_PATH = "img/cards"


@client.command()
async def cardlist(ctx, deck_name):
    try:
        cards = os.listdir("{}/{}".format(IMG_PATH, deck_name))
        output_msg = ""
        cards.sort()
        for card_name in cards:
            card_name = card_name.replace(".png", "")
            output_msg += "{}\n".format(card_name)

            if len(output_msg) > 1900:
                await ctx.channel.send(output_msg)
                output_msg = ""
        if output_msg != "":
            await ctx.channel.send(output_msg)
        await ctx.message.delete()
    except Exception as e:
        print(e)


@client.command()
async def decklist(ctx):
    try:
        decks = os.listdir(IMG_PATH)
        output_msg = ""
        decks.sort()
        for deck in decks:
            output_msg += "{}\n".format(deck)

            if len(output_msg) > 1900:
                await ctx.channel.send(output_msg)
                output_msg = ""
        if output_msg != "":
            await ctx.channel.send(output_msg)
        await ctx.message.delete()
    except Exception as e:
        print(e)


@client.command()
async def card(ctx, card_name):
    try:
        for (dirpath, dirnames, filenames) in os.walk(IMG_PATH):
            for filename in filenames:
                if filename == "{}.png".format(card_name):
                    await ctx.channel.send(file=File("{}/{}".format(dirpath, filename)))
        await ctx.message.delete()
    except Exception as e:
        print(e)


def main():
    client.run("NzI2NDM0MjkwODg3NDI2MTQ5.XvdOwQ.yZypDAtOxo5QPEkqOf1w9ZDtfwQ")


@client.event
async def on_ready():
    print("Guisebot connected")


if __name__ == "__main__":
    main()
