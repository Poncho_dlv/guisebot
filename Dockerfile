# Use the Python3.7
FROM python:3.7-buster

COPY ./app /app

# Set the working directory
WORKDIR /app

# Install the dependencies
RUN pip install -r requirements.txt